from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^lectures/', include('lectures.urls', namespace="lectures")),
    url(r'^admin/', include(admin.site.urls)),
)
